<?php
/**
 * @file
 * ict_basic_page.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function ict_basic_page_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__basic_page';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -99;
  $handler->conf = array(
    'title' => 'Basic Page',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'basic_page',
  );
  $display = new panels_display();
  $display->layout = 'bryant_flipped_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '113b7628-2c7f-4640-aee9-01780e0f064d';
  $display->content = array();
  $display->panels = array();
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view__basic_page'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function ict_basic_page_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'front';
  $page->task = 'page';
  $page->admin_title = 'front';
  $page->admin_description = '';
  $page->path = 'front';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_front__panel_context_46eb1b47-c135-4f9d-a4e9-5b75c77cd5d9';
  $handler->task = 'page';
  $handler->subtask = 'front';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(
      0 => array(
        'identifier' => 'Node',
        'keyword' => 'node',
        'name' => 'entity:node',
        'entity_id' => '1',
        'id' => 1,
      ),
    ),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'twocol_77_23_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'header' => NULL,
      'left' => NULL,
      'right' => NULL,
      'footer' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '1566ae6e-876e-49ef-be62-01231ac5ba34';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-37860417-554f-413d-9ec0-8a1eaa051ced';
    $pane->panel = 'left';
    $pane->type = 'node';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'nid' => '1',
      'links' => 0,
      'leave_node_title' => 0,
      'identifier' => '',
      'build_mode' => 'full',
      'link_node_title' => 0,
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '37860417-554f-413d-9ec0-8a1eaa051ced';
    $display->content['new-37860417-554f-413d-9ec0-8a1eaa051ced'] = $pane;
    $display->panels['left'][0] = 'new-37860417-554f-413d-9ec0-8a1eaa051ced';
    $pane = new stdClass();
    $pane->pid = 'new-e9f72d70-8243-4800-ba93-5faa6ee25d70';
    $pane->panel = 'left';
    $pane->type = 'block';
    $pane->subtype = 'bean-see-who-can-take-part';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'e9f72d70-8243-4800-ba93-5faa6ee25d70';
    $display->content['new-e9f72d70-8243-4800-ba93-5faa6ee25d70'] = $pane;
    $display->panels['left'][1] = 'new-e9f72d70-8243-4800-ba93-5faa6ee25d70';
    $pane = new stdClass();
    $pane->pid = 'new-54ca9d4c-0d74-440a-88a0-cf79442e5112';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'bean-about-the-study';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '54ca9d4c-0d74-440a-88a0-cf79442e5112';
    $display->content['new-54ca9d4c-0d74-440a-88a0-cf79442e5112'] = $pane;
    $display->panels['right'][0] = 'new-54ca9d4c-0d74-440a-88a0-cf79442e5112';
    $pane = new stdClass();
    $pane->pid = 'new-603ffe84-c5c6-4009-aa59-4114f75f7989';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'bean-faqs';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '603ffe84-c5c6-4009-aa59-4114f75f7989';
    $display->content['new-603ffe84-c5c6-4009-aa59-4114f75f7989'] = $pane;
    $display->panels['right'][1] = 'new-603ffe84-c5c6-4009-aa59-4114f75f7989';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['front'] = $page;

  return $pages;

}
