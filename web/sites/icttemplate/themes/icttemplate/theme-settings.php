<?php

/*
function icttemplate_form_system_theme_settings_alter(&$form, &$form_state) {
  // Add a checkbox to toggle the breadcrumb trail.
  //https://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/7
 ///dpm($form);
  $form['banner_image'] = array(
    '#type' => 'managed_file',
    '#name' => 'banner_image',
    '#title' => t('Banner image'),
    '#size' => 40,
    '#description' => t("Image should be less than 400 pixels wide and in JPG format."),
    '#upload_location' => 'public://'
  );
  $form['#submit'][] = 'icttemplate_system_theme_settings_submit';
  return $form;

}
/*
function icttemplate_system_theme_settings_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $values = $form_state['values'];
/*  // Exclude unnecessary elements before saving.
  form_state_values_clean($form_state);

  $values = $form_state['values'];

  // Extract the name of the theme from the submitted form values, then remove
  // it from the array so that it is not saved as part of the variable.
  $key = $values['var'];
  unset($values['var']);

  // If the user uploaded a new logo or favicon, save it to a permanent location
  // and use it in place of the default theme-provided file.
  if (!empty($values['logo_upload'])) {
    $file = $values['logo_upload'];
    unset($values['logo_upload']);
    $filename = file_unmanaged_copy($file->uri);
    $values['default_logo'] = 0;
    $values['logo_path'] = $filename;
    $values['toggle_logo'] = 1;
  }
  if (!empty($values['favicon_upload'])) {
    $file = $values['favicon_upload'];
    unset($values['favicon_upload']);
    $filename = file_unmanaged_copy($file->uri);
    $values['default_favicon'] = 0;
    $values['favicon_path'] = $filename;
    $values['toggle_favicon'] = 1;
  }

  // If the user entered a path relative to the system files directory for
  // a logo or favicon, store a public:// URI so the theme system can handle it.
  if (!empty($values['logo_path'])) {
    $values['logo_path'] = _system_theme_settings_validate_path($values['logo_path']);
  }
  if (!empty($values['favicon_path'])) {
    $values['favicon_path'] = _system_theme_settings_validate_path($values['favicon_path']);
  }

  if (empty($values['default_favicon']) && !empty($values['favicon_path'])) {
    $values['favicon_mimetype'] = file_get_mimetype($values['favicon_path']);
  }

  variable_set($key, $values);
  drupal_set_message(t('The configuration options have been saved.'));

  cache_clear_all();

  if (isset($values['banner_image'])) {
    $file = file_load($values['banner_image']);

    $file->status = FILE_STATUS_PERMANENT;

    file_save($file);
  }
}
*/
