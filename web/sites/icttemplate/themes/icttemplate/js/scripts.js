(function($) {

  var desk = 70;
  var tab = 50;
  var navigation;
  var previousState;

  function widthToEm(width) {
    return (width / parseFloat($("body").css("font-size")));
  }

  //Initiate Burger Control
  Drupal.behaviors.icttemplate_menu_burger_behavior = {
    attach: function(context, settings) {
      $('.l-region--navigation #main-menu-burger', context).once('burger', function() {
        $(this).click(function() {
          $('#block-system-main-menu').slideToggle('slow');
        });
      });
    }
  };

  function resize_menu() {
    if (widthToEm($(window).width()) < tab) {
      $('#block-system-main-menu').css('display', 'none');
      $('#main-menu-burger').css('display', 'block');
    } else {
      $('#block-system-main-menu').css('display', 'block');
      $('#main-menu-burger').css('display', 'none');
    }
  }
  resize_menu();
  $(window).resize(function() {
    resize_menu();
  });


    $(document).ready(function() {
        $(".button").wrap( "<div class='cube'></div>" );
        $('.button').clone().appendTo('.cube');
        $('.button:first-child').remove();
        $('.button:last-child').addClass('button-flip').removeClass('button');
        console.log('testing');
    });

    $(document).ready(function() {
        //$('.node.node--question').hide();
        console.log('answer');
        //all links that start with "http" should open in a new browser window
        $('a[href^="http"]').attr('target', '_blank');

        $(".pane-title").click(function() {

            $(this).parent().children(".node--question").slideToggle(500); });
    });

})(jQuery);