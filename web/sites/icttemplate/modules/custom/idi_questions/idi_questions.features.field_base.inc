<?php
/**
 * @file
 * idi_questions.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function idi_questions_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_answer'
  $field_bases['field_answer'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_answer',
    'format' => 'full_html',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  return $field_bases;
}
