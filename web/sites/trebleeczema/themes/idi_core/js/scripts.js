(function($) {

  var desk = 70;
  var tab = 44;
  var navigation;
  var previousState;



  function widthToEm(width) {

    return (width / parseFloat($("body").css("font-size")));

  }

 //Initiate Main Menu
  Drupal.behaviors.idi_core_menu_behavior = {
    attach: function(context, settings) {
      $('#block-system-main-menu', context).once('main-menu', function() {
        //Menu Functions Here
      });
    }
  };

  //Initiate Secondary Menu
  Drupal.behaviors.idi_core_secondary_menu_behavior = {
    attach: function(context, settings) {
      $('#block-menu-block-1', context).once('secondary-menu', function() {
        //Menu Functions Here
         $menu = $('#block-menu-block-1');
         $menu.find('ul ul').each(function(){
            $(this).parent('li').append('<span class="toggle">&gt;</span>');

            });
          $menu.find('ul li span.toggle').click(function(){
              console.log('toggle clicked');
              if($(this).parent('li').hasClass('activated')){
                $(this).parent('li').toggleClass('activated').children('ul').hide();
              }else{
                $(this).parent('li').toggleClass('activated').children('ul').show();
              }

          });

      });
    }
  };

//Initiate Burger Control
 /* Drupal.behaviors.idi_core_menu_burger_behavior = {
    attach: function(context, settings) {

      $('.l-region--navigation #main-menu-burger', context).once('burger', function() {



        $(this).click(function() {

          $('#block-system-main-menu, #block-views-category-nav-block').toggle();

        });

      });
    }
  };*/

//Initiate Burger Control
  Drupal.behaviors.idi_core_menu_responsive_nav_behavior = {
    attach: function(context, settings) {

      $('#block-system-main-menu', context).once('responsive-nav', function() {
          var $menu = $('#block-system-main-menu');
          var $menuClone = $menu.clone();
          $menuClone.find('.contextual-links-wrapper, .menu-views').remove();
          //$menuClone.children('ul.menu').unwrap();
          $menuClone.attr('id', 'resp-nav').removeClass().appendTo('body');

          $menuClone.find('ul ul').each(function(){
            $(this).parent('li').append('<span class="toggle">&gt;</span>');

            });
          //$menuClone.find('ul li:not(ul ul li)').append('<span class="toggle">Toggle</span>');
          $menuClone.find('ul li span.toggle').click(function(){

              if($(this).parent('li').hasClass('activated')){                $
                $(this).parent('li').toggleClass('activated').children('ul').hide();
              }else{
                $(this).parent('li').toggleClass('activated').children('ul').show();
              }

          });

          $searchClone = $('#block-search-form').clone();
          $searchClone.attr('class', 'resp-search').prependTo($menuClone).css({'display':'block'});


          //Normal non mobile flyouts
          $menu.find('ul.menu li').hover(
          //IN
          function(){
            $(this).children('ul').stop(true, true).toggle(300);
          },
          //OUT
          function(){
            $(this).children('ul').stop(true, true).toggle(300);
          });

      });
    }
  };

//Initiate Resize Functions
  Drupal.behaviors.idi_core__on_resize_behavior = {
    attach: function(context, settings) {

      var sidrInitiated = false;
      //var menuHidden = false;

      $('body', context).once('resize', function() {

        if (widthToEm($(window).width()) > tab) {
          previousState = 'desktop';
          //menuHidden = false;

        } else {
          previousState = 'mobile';
          //$('#block-system-main-menu')
          $('#main-menu-burger').sidr({
              name: 'resp-nav',
              side: 'left' // By default
          });
          sidrInitiated = true;
          //menuHidden = true;

        }
        $(window).resize(function() {

          if (this.resizeTO)
            clearTimeout(this.resizeTO);
          this.resizeTO = setTimeout(function() {
            $(this).trigger('resizeEnd');
          }, 500);

        });

        $(window).bind('resizeEnd', function() {
          //do something, window hasn't changed size in 500ms
          //   console.log('window resized');
          if (widthToEm($(window).width()) > tab && previousState == 'mobile') {
            //   console.log('switching to desktop');
            previousState = 'desktop';

            if(sidrInitiated == true){
                jQuery.sidr('close', 'resp-nav');
            }

            //if(menuHidden == true){
            //$('#block-system-main-menu, #block-views-category-nav-block').show();
            //$('.l-region--navigation #main-menu-burger').hide();



            //}
            //setupDesktopNav();
          } else if (widthToEm($(window).width()) < tab && previousState == 'desktop') {
            previousState = 'mobile';
            //setupMobileNav();
            //if(menuHidden == false){
            //     console.log('switching to mobile');

             if(sidrInitiated == false){
                $('#main-menu-burger').sidr({
                  name: 'resp-nav',
                  side: 'left' // By default
                });
                sidrInitiated = true;
            }
            //menuHidden = true;
            //$('#block-system-main-menu, #block-views-category-nav-block').hide();
            //$('.l-region--navigation #main-menu-burger').show();
            //}

          }
        });

      });
    }
  };





})(jQuery);
