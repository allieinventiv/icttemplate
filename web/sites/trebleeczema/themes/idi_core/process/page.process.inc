<?php

/**
 * Implements hook_process_page().
 */
function idi_core_process_page(&$variables) {
  // You can use process hooks to modify the variables before they are passed to
  // the theme function or template file.
     $variables['title'] = make_it_super($variables['title']);

}
