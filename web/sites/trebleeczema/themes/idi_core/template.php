<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the theme.
 */


//PULL IN HELPER FUNCTIONS
 include_once "theme_helpers.php";

function idi_core_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
  //dpm($element);
  if(isset($element['#localized_options']['attributes']['id'])){
      if($element['#localized_options']['attributes']['id'] == 'forward'){
          $element['#href'] = 'forward';
          $element['#localized_options']['query'] = array('path' => current_path());
      }
  }
  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }


  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  //Make Reg and stuff Super
  $output = make_it_super($output);

  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/*
function idi_core_form_alter(&$form, &$form_state, $form_id) {

  if ($form_id == 'search_block_form') {
    $form['search_block_form']['#title'] = t('poop'); // Change the text on the label element
    $form['search_block_form']['#title_display'] = 'invisible'; // Toggle label visibilty
    $form['search_block_form']['#size'] = 40;  // define size of the textfield
    $form['search_block_form']['#default_value'] = t('Search'); // Set a default value for the textfield
    $form['actions']['submit']['#value'] = t('GO!'); // Change the text on the submit button
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => 'search');

    // Add extra attributes to the text box
    $form['search_block_form']['#attributes']['onblur'] = "if (this.value == '') {this.value = 'Search';}";
    $form['search_block_form']['#attributes']['onfocus'] = "if (this.value == 'Search') {this.value = '';}";
    // Prevent user from searching the default text
    $form['#attributes']['onsubmit'] = "if(this.search_block_form.value=='Search'){ alert('Please enter a search'); return false; }";

    // Alternative (HTML5) placeholder attribute instead of using the javascript
    $form['search_block_form']['#attributes']['placeholder'] = t('Search');
  }
}


*/


function idi_core_search_module_menu_alter(&$items) {
  drupal_static_reset('search_get_info');
  $default_info = search_get_default_module_info();
  if ($default_info) {
    foreach (search_get_info() as $module => $search_info) {
      $path = 'result/' . $search_info['path'];
      unset($items[$path]);
      unset($items["$path/%menu_tail"]);
    }

    $items['search/%menu_tail'] = array(
      'title' => 'Search',
      'load arguments' => array('%map', '%index'),
      'page callback' => 'search_view',
      'page arguments' => array($default_info['module'], 1),
      'access callback' => '_search_menu_access',
      'access arguments' => array($default_info['module']),
      'type' => MENU_CALLBACK,
      'file' => drupal_get_path('module', 'result') . '/search.pages.inc'
    );
  }
}



function idi_core_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  //dpm($breadcrumb);
  if (!empty($breadcrumb)) {
      $crumbs = '<div class="breadcrumbs clearfix"><ul>';

      foreach($breadcrumb as $value) {

         //Make Reg and stuff Super
         //$value['#title'] = make_it_super($value);

         if($value == '<a href="/">Home</a>'){

            $crumbs .= '<li class="home">'.$value.' &gt;</li>';

         }else if (strpos($value, 'href="') == FALSE && strpos($value, 'Disciplines') === TRUE ){

            $crumbs .= '<li>'.$value.'</li>';

         }else{

           $crumbs .= '<li>'.$value.' &gt;</li>';

         }
           //dpm($value);
      }
      $url_alias = drupal_get_path_alias($_GET['q']);
      $split_url = explode('/', $url_alias);
      if($split_url[0] != 'disciplines'){
        $crumbs .= '<li>' . Truncate(str_replace("®", "<sup>&reg;</sup>", drupal_get_title()), 35, false) .'</li>';
      }
      $crumbs .= '</ul></div>';

      return $crumbs;
    }

  }


function idi_core_menu_tree__main_menu(&$variables){

  drupal_add_js(libraries_get_path('sidr', FALSE) . '/jquery.sidr.min.js', array('type' => 'file', 'scope' => 'footer', 'weight' => 5));
  //drupal_add_css(libraries_get_path('sidr', FALSE) . '/stylesheets/jquery.sidr.dark.css');

  return '<ul class="menu">' . $variables['tree'] . '</ul>';

}