
#Common modules
#cd $SITE_DIR/web
drush dl -n module_filter clean_markup smtp advanced_help ctools date views ds devel variable features libraries menu_block menu_attributes redirect globalredirect services uuid flag entity_view_mode token transliteration jquery_update views_bulk_operations metatag metatags_quick email taxonomy_menu webform pathauto block_class menu_item_visibility entityreference
drush -y en module_filter advanced_help smtp ctools features
#drush -y en clean_markup clean_markup_blocks
drush -y en date date_api date_popup date_repeat date_repeat_field date_all_day date_tools date_views date_views  entityreference
#=======

#Disable comment
drush -y dis comment

#CTools
drush dl -n ctools
drush -y en ctools page_manager bulk_export

#Entity
drush dl -n entity entitycache
drush -y en entity entity_token entitycache

#Libraries
drush dl -n libraries
drush -y en libraries

#Views
drush dl -n views views_bulk_operations views_litepager entity_view_mode
drush -y en views views_ui views_bulk_operations entity_view_mode

#Date
drush dl -n date
drush -y en date date_api date_popup date_repeat date_repeat_field date_all_day date_tools date_views date_views

#Devel
drush dl -n devel
drush -y en devel devel_generate

#Display Suite
drush dl -n ds
drush -y en ds ds_ui ds_search ds_forms ds_format ds_extras ds_devel

#Menus
drush dl -n menu_block menu_attributes taxonomy_menu block_class menu_item_visibility special_menu_items
drush -y en menu_block menu_attributes taxonomy_menu block_class menu_item_visibility special_menu_items

drush dl -n module_filter token pathauto smtp advanced_help variable redirect globalredirect services uuid flag transliteration email
drush -y en module_filter token pathauto smtp advanced_help variable redirect globalredirect services uuid flag transliteration email

#Blocks
drush dl -n blockreference
drush -y en blockreference


#Fields
drush dl -n field_collection field_group
drush -y en field_collection field_group

#jQuery Update
drush dl -n jquery_update
drush -y en jquery_update
drush vset --always-set jquery_update_jquery_version '1.8'

#Webforms
drush dl -n webform
drush -y en webform
drush vset --always-set webform_default_from_address ''

#Features
drush dl -n features fe_block strongarm
drush -y en features fe_block strongarm

#Content editing
drush dl -n link insert media media_youtube
drush -y en link insert media media_internet media_youtube

#CKEditor - we don't use WYSIWYG editors on most sites
#drush dl -n ckeditor ckeditor_link
#drush -y en ckeditor ckeditor_link

#https://drupal.org/project/diff
drush dl -n diff
drush -y en diff

#Admin Menu
drush dl -n admin_menu
drush -y dis toolbar overlay
drush -y en admin_menu_toolbar

#SEO - disabled for now
#drush -y en page_title seotools seo_checklist
drush -y en metatag metatag_views metatag_opengraph

#Panels
drush dl -n panels panelizer
drush -y en panels panels_node panels_mini panels_ipe

#Metatags
drush dl -n metatags metatags_quick metatags_panels
drush -y en metatag_views, metatag_ui, metatag_twitter_cards, metatag_panels, metatag_opengraph, metatag_devel, metatag_dc, metatag_context, metatag

#drush dl -n architecture
#drush -y en architecture

#Security
drush dl -n seckit username_enumeration_prevention password_policy securepages security_review
drush -y en seckit username_enumeration_prevention password_policy security_review password_policy_password_tab

#XMLSitemaps
mkdir -p $SITE_DIR/web/sites/default/files/xmlsitemap
echo "*" > $SITE_DIR/web/sites/default/files/xmlsitemap/.gitignore

#Security fix https://drupal.org/SA-CORE-2013-003
sudo cp $SITE_DIR/web/sites/default/files/.htaccess /tmp/
