<?php
// Author: Kazaam Interactive
$doRedirect = FALSE;

//
// taken from and modified
// http://www.russellbeattie.com/blog/mobile-browser-detection-in-php
$op = strtolower($_SERVER['HTTP_X_OPERAMINI_PHONE']);
$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
$ac = strtolower($_SERVER['HTTP_ACCEPT']);
$ip = $_SERVER['REMOTE_ADDR'];

$ua_mobile_keywords = array(
    "android","blackberry","googlebot-mobile","iemobile","iphone","ipod","opera mobile","palmos","webos",
    "sony",
    "symbian",
    "nokia",
    "samsung",
    "mobile",
    "windows ce",
    "epoc",
    "opera mini",
    "nitro",
    "j2me",
    "midp-",
    "cldc-",
    "netfront",
    "mot",
    "up.browser",
    "up.link",
    "audiovox",
    "blackberry",
    "ericsson,",
    "panasonic",
    "philips",
    "sanyo",
    "sharp",
    "sie-",
    "portalmmm",
    "blazer",
    "avantgo",
    "danger",
    "palm",
    "series60",
    "palmsource",
    "pocketpc",
    "smartphone",
    "rover",
    "ipaq",
    "au-mic,",
    "alcatel",
    "ericy",
    "up.link",
    "vodafone/",
    "wap1.",
    "wap2."
);

$ua_accept_mobile_keywords = array("ipad");

// WAP or OPERA mini
$isMobile = strpos($ac, 'application/vnd.wap.xhtml+xml') !== false || $op != '';
if ($isMobile == false)
{
    foreach ($ua_mobile_keywords as $keyword)
    {
        $isMobile |= strpos(strtolower($ua), $keyword) !== false;
        if ($isMobile == true) {
            break;
        }
    }

    //For excepted devices
    foreach ($ua_accept_mobile_keywords as $keyword)
    {
        if(strpos(strtolower($ua), $keyword) !== false){
            $isMobile = false;
            break;
        }
    }
}

// environments
$env_dev = 'dev'; $env_stage = 'staging'; $env_prod = 'production';

$fullSite = array($env_dev => 'http://acoresandbox.dev/',
  $env_stage => 'http://acoresandbox.kazaamweb.com/',
  $env_prod => 'http://www.acoresandbox.com/');

$mobileSite = array($env_dev => 'http://m.acoresandbox.dev/',
  $env_stage => 'http://m.acoresandbox.kazaamweb.com/',
  $env_prod => 'http://m.acoresandbox.com/');

// redirect to approprite site
if ($doRedirect)
{
  // currentUri
  $currentUri = str_replace('?', '', str_replace($_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']));
  $currentUri = "http://" . $_SERVER['HTTP_HOST'] . $currentUri;

  $needFullsite = $isMobile;
  $redirectTo = FALSE;

  // on a mobile device
  if ($isMobile == true)
  {
    // check if fullsite to mobile
    foreach ($fullSite as $env => $url)
    {
      if (stripos($currentUri, $url) !== false)
      {
        // redirect to mobile site; if mobile device and have mobile site for same env;
        if (array_key_exists($env, $mobileSite) && isset($mobileSite[$env]))
        {
          $redirectTo = $mobileSite[$env];
        }
      }
    }
  }

  // NOT on a mobile device
  else //if ($isMobile == false)
  {
    // check if mobile to fullsite
    foreach ($mobileSite as $env => $url)
    {
      if (stripos($currentUri, $url) !== false)
      {
        // redirect to full site; if on a PC browser and have a full site for same env;
        if (array_key_exists($env, $fullSite) && isset($fullSite[$env]))
        {
          $redirectTo = $fullSite[$env];
        }
      }
    }
  }


  // do redirect;
  if ($redirectTo !== FALSE && filter_var($redirectTo, FILTER_VALIDATE_URL) !== false)
  {
    $doRedirect = TRUE;
    if ($needFullsite == true)
    {
      // DEBUG ONLY
      //$_SESSION["fullsite"] = FALSE;
      // DEBUG ONLY

      // if request is to show fullsite?
      if (isset($_REQUEST['fullsite']))
      {
          $_SESSION["fullsite"] = TRUE;
      }

      if (isset($_SESSION["fullsite"]) && $_SESSION["fullsite"] === TRUE)
      {
          // ; do nothing ; allow access
        $doRedirect == FALSE;
      }
    }

    // handle redirect
    if ($doRedirect == TRUE)
    {
      header("HTTP/1.1 302 Found");
      header("Location: " . $redirectTo);
      exit;
    }
  }
}

?>
