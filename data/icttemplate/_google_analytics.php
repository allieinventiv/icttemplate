<script type="text/javascript">
  /**
   * Record a hit to Google Analytics for ajax-loaded pages.
   *
   * Reference: https://developers.google.com/analytics/devguides/collection/analyticsjs/field-reference#create
   */
  function trackPageView() {
    try {
      //log('trackPageView(' + location.href + ')', LOG_DEBUG);
      ga('send', 'pageview'/*, {
        //'page': '/page-uri',
        //'title': 'Page Title',
        'hitCallback': function() {
          //alert('analytics.js done sending data');
        }
      }*/);
    } catch (ex) {
      alert('trackPageView error: ' + ex);
    }
  }

  /**
   * Record an event to Google Analytics (https://jira.kazaamweb.com/browse/ZPR-94)
   *
   * Reference: https://developers.google.com/analytics/devguides/collection/analyticsjs/field-reference#create
   */
  function trackEvent(category, action, label/*, value, noninteraction*/) {
    try {
      //ga('set', 'nonInteraction', noninteraction);
      ga('send', 'event', {
        'eventCategory': category,
        'eventAction': action,
        'eventLabel': label
          //,'eventValue': value
      });
      setTimeout(function() {}, 350); //slow down link click
    } catch (ex) {
      alert('_trackEvent error: ' + ex);
    }
  }
</script>

<?php
$ga_account = null;
if (preg_match("/acoresandbox\.com$/", $_SERVER["SERVER_NAME"]) || preg_match("/prod\.acoresandbox\.kazaamweb\.com$/", $_SERVER["SERVER_NAME"]))
{
  $ga_account = "";
}
?>

<?php if ($ga_account): ?>
  <!-- Google Analytics -->
  <script type="text/javascript">

    // https://developers.google.com/analytics/devguides/collection/analyticsjs/
    (function(i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function() {
        (i[r].q = i[r].q || []).push(arguments)
      }, i[r].l = 1 * new Date();
      a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    //https://developers.google.com/analytics/devguides/collection/analyticsjs/field-reference#create
    ga('create', '<?php echo $ga_account; ?>', {
      //'alwaysSendReferrer': true, //Enable this setting only if you want to process other pages from your current host as referrals.
      //'siteSpeedSampleRate': 50, //Site Speed Sample Rate; default is 1%, 50 is 50%
      'cookieDomain': '<?php echo $_SERVER["SERVER_NAME"]; ?>',
      'cookieName': '<?php echo str_replace('.', '_', $_SERVER["SERVER_NAME"]); ?>_ga',
      //'cookieExpires': 86400, // in seconds, default is 63072000 (2 years)
      'hitCallback': function() {
        //alert('analytics.js done sending data');
      }
    });

    // track page view
    trackPageView();

  </script>
  <!-- End Google Analytics -->
<?php else: ?>
  <!--Google Analytics will be here in production -->
  <script type="text/javascript">
    var ga = function(command, action, data) {
      // stub
    };
    trackPageView();
  </script>
<?php endif; ?>
